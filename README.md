**GUIDE FOR THE MAKEFILE**
*  `make`        => create a class folder and compile all `.java` files in this folder
*  `make run`    => if all classes were already created, run the program
*  `make b-run`  => compile all classes then automatically run the program
*  `make clean` => rm -fr class folder

<br/>

**GUIDE FOR THE GAME**

There is no particular indication for the terminal part because you are guided through the whole thing.

For the GUI part, you just have to click on the tile you want to pick (it will automatically take all the tiles of the same color) then click where you want to put it.
The player whose background becomes purple is the one playing.