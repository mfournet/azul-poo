package view.src;

import datamodel.src.*;

import javax.swing.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import static datamodel.src.Azul.g1;

public class MouseEvents {
    public static int turn = 0;

    //creation of mouseClick event for center

    static MouseListener clickedCenter = new MouseListener() {
        @Override
        public void mouseClicked(MouseEvent mouseEvent) {
            //On r�cup�re le JComponent
            JComponent lab = (JComponent) mouseEvent.getSource();
            //Du composant, on r�cup�re l'objet de transfert : le nôtre
            TransferHandler handle = lab.getTransferHandler();
            handle.exportAsDrag(lab, mouseEvent, TransferHandler.NONE);

            try {
                Game.getPlayers()[turn].handStock = Game.getShared().getCenter().take(TType.backGroundToTType(lab.getBackground()));
                GameView.gameViewMaj();
            } catch (Case.UnableToPutTileInCase unableToPutTileInCase) {
            }
            GameView.gameViewMaj();
        }

        @Override
        public void mousePressed(MouseEvent mouseEvent) {

        }

        @Override
        public void mouseReleased(MouseEvent mouseEvent) {

        }

        @Override
        public void mouseEntered(MouseEvent mouseEvent) {

        }

        @Override
        public void mouseExited(MouseEvent mouseEvent) {

        }
    };
    //creation of mouseClick event for fabric

    static MouseListener clickedFabrics = new MouseListener() {
        @Override
        public void mouseClicked(MouseEvent mouseEvent) {
            //On r�cup�re le JComponent
            JComponent lab = (JComponent) mouseEvent.getSource();
            //Du composant, on r�cup�re l'objet de transfert : le nôtre

            Game.getPlayers()[turn].handStock = Game.getShared().getFabrics()[Integer.parseInt(lab.getParent().getName())].take(TType.backGroundToTType(lab.getBackground()));

            for(int i = 0; i <Game.getPlayers()[turn].handStock.size(); i++ ) {
                System.out.println(Game.getPlayers()[turn].handStock.get(i).getType());
            }
            GameView.gameViewMaj();
        }

        @Override
        public void mousePressed(MouseEvent mouseEvent) {

        }

        @Override
        public void mouseReleased(MouseEvent mouseEvent) {

        }

        @Override
        public void mouseEntered(MouseEvent mouseEvent) {

        }

        @Override
        public void mouseExited(MouseEvent mouseEvent) {


        }
    };

    static MouseListener receiveLine = new MouseListener() {
        @Override
        public void mouseClicked(MouseEvent mouseEvent) {
            //On r�cup�re le JComponent
            JComponent lab = (JComponent) mouseEvent.getSource();
            //Du composant, on r�cup�re l'objet de transfert : le n�tre
            
            System.out.println("handStock Put:");
            for(int i = 0; i <Game.getPlayers()[turn].handStock.size(); i++ ) {
                System.out.println(Game.getPlayers()[turn].handStock.get(i).getType());
            }
            try {
                Game.getPlayers()[turn].getLines().put(Game.getPlayers()[turn].handStock, Integer.parseInt(lab.getName()));
                Game.getPlayers()[turn].handStock = null;
                turn = (turn + 1) % Game.getPlayers().length;
            } catch (Case.UnableToPutTileInCase | Lines.WrongSelectionException | NullPointerException | ArrayIndexOutOfBoundsException unableToPutTileInCase) {
                //unableToPutTileInCase.printStackTrace();
            }
            //Game.getPlayers()[turn].endOfTurn();
            Game.getShared().cleanAll();

            if(g1.isEmpty()) {
                try {
                    g1.endOfTurn();
                    g1.buildWall();
                    if(g1.isGameEnded()) {
                        GameView.gameViewMaj();
                        g1.gameEnd(0);
                        System.exit(0);
                    }

                } catch (Case.UnableToPutTileInCase e) {
                    System.out.print("There was a problem during your turn, the game can't proceed.");
                    e.printStackTrace();
                    System.exit(1);
                }
                //turn = g1.endOfTurn();
                g1.fill();

                //g1.isEnded();
            }


            //new GameView(Game.getPlayers().length);
            GameView.gameViewMaj();

            //lab.setBackground(Color.BLACK);

        }

        @Override
        public void mousePressed(MouseEvent mouseEvent) {

        }

        @Override
        public void mouseReleased(MouseEvent mouseEvent) {

        }

        @Override
        public void mouseEntered(MouseEvent mouseEvent) {

        }

        @Override
        public void mouseExited(MouseEvent mouseEvent) {

        }
    };

    static MouseListener receiveFloor= new MouseListener() {
        @Override
        public void mouseClicked(MouseEvent mouseEvent) {
            try {
                Game.getPlayers()[turn].getFloor().put(Game.getPlayers()[turn].handStock);
                Game.getPlayers()[turn].handStock = null;
                turn = (turn + 1) % Game.getPlayers().length;
            } catch (Case.UnableToPutTileInCase | NullPointerException | ArrayIndexOutOfBoundsException | Floor.FloorIsFull unableToPutTileInCase) {
            }
            Game.getShared().cleanAll();

            if(g1.isEmpty()) {
                try {
                    g1.endOfTurn();
                    g1.buildWall();
                    if(g1.isGameEnded()) {
                        GameView.gameViewMaj();
                        g1.gameEnd(0);
                        System.exit(0);
                    }

                } catch (Case.UnableToPutTileInCase e) {
                    System.out.print("There was a problem during your turn, the game can't proceed.");
                    e.printStackTrace();
                    System.exit(1);
                }
               // turn = g1.endOfTurn();
                g1.fill();

                //g1.isEnded();
            }


            //new GameView(Game.getPlayers().length);
            GameView.gameViewMaj();

            //lab.setBackground(Color.BLACK);

        }

        @Override
        public void mousePressed(MouseEvent mouseEvent) {

        }

        @Override
        public void mouseReleased(MouseEvent mouseEvent) {

        }

        @Override
        public void mouseEntered(MouseEvent mouseEvent) {

        }

        @Override
        public void mouseExited(MouseEvent mouseEvent) {

        }
    };



}
