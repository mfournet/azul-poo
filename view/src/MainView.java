package view.src;

import datamodel.src.*;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.Color;

public class MainView {
    private JPanel mainPanel;
    private JPanel titleContainer;
    private JPanel comboBoxContainer;
    private JPanel buttonContainer;
    private JComboBox<Integer> player;
    private JComboBox<String> gamemode;


    public MainView() {
        JFrame frame = new JFrame("Retro Azul Start");
        frame.setAutoRequestFocus(true);
        mainPanel = new JPanel();
        mainPanel.setLayout(new GridLayout(3,0));
        mainPanel.setBackground(new Color(0x503538));
        frame.add(mainPanel);
        //titlecontainer
        titleContainer = new JPanel();
        mainPanel.add(titleContainer);
        titleContainer.setBackground(new Color(0x503538));
        //title
            JLabel title = new JLabel("RETRO AZUL");
            title.setForeground(Color.WHITE);
            title.setFont(new Font("Serif", Font.BOLD, 45));
            title.setBorder(new EmptyBorder(30, 0, 0, 0));
            titleContainer.add(title);

        //comboBoxContainer
        comboBoxContainer = new JPanel();
        comboBoxContainer.setLayout(new FlowLayout());
        comboBoxContainer.setBackground(new Color(0x503538));

        mainPanel.add(comboBoxContainer, BorderLayout.CENTER);
            //combobox players text
            JLabel comboBoxPlayerText = new JLabel("Number of players :");
            comboBoxPlayerText.setForeground(Color.WHITE);
            comboBoxPlayerText.setFont(new Font("Serif", Font.PLAIN, 20));
            comboBoxContainer.add(comboBoxPlayerText);
            //combobox players
            Integer[] p = {2,3,4};
            player = new JComboBox<Integer>(p);
            comboBoxContainer.add(player);

            //combobox mode text
            JLabel comboBoxModeText = new JLabel("Choose your mode :");
            comboBoxModeText.setForeground(Color.WHITE);
            comboBoxModeText.setFont(new Font("Serif", Font.PLAIN, 20));

            comboBoxContainer.add(comboBoxModeText);

            //combobox mode
            String[] gm = {"Normal","Extension"};
            gamemode = new JComboBox<String>(gm);

            comboBoxContainer.add(gamemode);

        //button container
        buttonContainer = new JPanel();
        buttonContainer.setBackground(new Color(0x503538));

        mainPanel.add(buttonContainer);
            //button
            JButton button = new JButton("Play");
            button.setForeground(Color.BLACK);
            button.setBackground(Color.WHITE);
            Border line = new LineBorder(Color.BLUE);
            Border margin = new EmptyBorder(5, 15, 5, 15);
            Border compound = new CompoundBorder(line, margin);
            button.setBorder(compound);
            button.setOpaque(true);
            button.addActionListener(actionEvent -> {
                Player[] players = new Player[(int)player.getSelectedItem()];
                for(int i = 0; i < (int)player.getSelectedItem();i++ ) {
                    players[i] = new Player("Player "+(i+1));
                }
                Azul.g1 = new Game(players);
                MainView.runGame();
                frame.dispose();
            });
            buttonContainer.add(button);


        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setSize(800, 600);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    public static void runGame() {
        new GameView();
    }
}
