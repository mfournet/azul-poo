package view.src;

import datamodel.src.*;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.Color;


public class GameView extends JFrame {
	private static final long serialVersionUID = 6181129767758947515L;
	
	static JFrame gameView;
    static JPanel mainContainer;
    static JPanel fabricsContainer;
    static int nBfabrics;
    static JPanel centerPanel;
    static JPanel floorPanel;
    static JPanel wallPanel;
    static JPanel linePanel;


    public GameView() {
        this.setBackground(Color.lightGray);
        this.setTitle("Azul Game");
        this.setSize(1400, 700); //fenetre.pack();
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        gameView =this;
        //mainContainer
        mainContainer = new JPanel();
        mainContainer.setPreferredSize(new Dimension(1400, 900));
        mainContainer.setLayout(new GridLayout(3, 0));
        mainContainer.setBackground(new Color(0x503538));
        this.add(mainContainer);

        //fabrics
        datamodel.src.Fabric[] fabrics = Game.getShared().getFabrics();
        nBfabrics = fabrics.length;

        fabricsContainer = new Fabric();
        fabricsContainer.setBackground(new Color(0x503538));
        fabricsContainer.setLayout(new GridBagLayout());
        fabricsContainer.setMaximumSize(new Dimension(200, 300));
        mainContainer.add(fabricsContainer);

        //center
        centerPanel = new Center();
        mainContainer.add(centerPanel, BorderLayout.CENTER);
        centerPanel.setBorder(new EmptyBorder(70, 70, 70, 70));


        //Player panel
        JPanel player = new JPanel();
        player.setLayout(new GridLayout(0, Azul.getPlayers().length));
        player.setSize(300,800);
        player.setBackground(new Color(0x503538));
        mainContainer.add(player);

        //player
       for(int i = 0; i < Azul.getPlayers().length; i++) {
           JPanel playerLayout = new JPanel();
           playerLayout.setOpaque(false);
           playerLayout.setEnabled(true);
           playerLayout.setSize(new Dimension(200, 500));
           playerLayout.setMinimumSize(new Dimension(200, 500));
           playerLayout.setLocation(new Point());
           playerLayout.setLayout(new GridLayout(3, 2, 2, 2));
           playerLayout.setBorder(BorderFactory.createEmptyBorder(10, 20, 10, 20));
           player.add(playerLayout);
           //player title
           JPanel playerTitle = new JPanel();
           playerTitle.setLayout(new GridLayout(2, 0));
           if( Azul.getPlayers()[i] == Game.getPlayers()[MouseEvents.turn]) {
               playerTitle.setBackground(new Color(0x8c7ae6));
           }else {
               playerTitle.setBackground(Color.lightGray);
           }
           playerTitle.setBounds(2,3,3,3);
           JLabel name = new JLabel(Azul.getPlayers()[i].getName());
           name.setFont(new Font("Montserrat", Font.BOLD, 18));
           name.setBorder(BorderFactory.createEmptyBorder(5, 20, 0, 0));

           playerTitle.add(name);
           name.setVerticalAlignment(JLabel.NORTH);

           JLabel score = new JLabel(String.valueOf(Azul.getPlayers()[i].getScore()));
           playerTitle.add(score);
           score.setVerticalAlignment(JLabel.NORTH);
           score.setBorder(BorderFactory.createEmptyBorder(0, 20, 0, 0));

           score.setFont(new Font("Montserrat", Font.PLAIN, 23));


           //Line-Wall
           JPanel lineWallContainer = new JPanel();

           lineWallContainer.setLayout(new GridLayout(0, 3));
           playerLayout.add(lineWallContainer,BorderLayout.SOUTH );
           lineWallContainer.setBounds(2,3,3,3);
           lineWallContainer.add(playerTitle);

           //Line
           linePanel = new Line(Azul.getPlayers()[i]);
           lineWallContainer.add(linePanel, BorderLayout.WEST);

           //Wall
           wallPanel = new Wall(Azul.getPlayers()[i]);
           wallPanel.setSize(300,300);
           lineWallContainer.add(wallPanel, BorderLayout.EAST);


           //Floor

           floorPanel = new JPanel();
           floorPanel.setLayout(new GridLayout(0,2));

           floorPanel.add(new Hand(Azul.getPlayers()[i]));
           floorPanel.add(new Floor(Azul.getPlayers()[i]));

           floorPanel.setBorder(BorderFactory.createEmptyBorder(5, 10, 5, 10));
           playerLayout.setPreferredSize(new Dimension(250, 250));
           playerLayout.add(floorPanel, BorderLayout.SOUTH);


           name.setBackground(Color.lightGray);
           score.setBackground(Color.lightGray);
           lineWallContainer.setBackground(Color.lightGray);
           linePanel.setBackground(Color.lightGray);
           wallPanel.setBackground(Color.lightGray);
           floorPanel.setBackground(Color.lightGray);


       }

        this.setResizable(false);
        this.setVisible(true);
    }

    public static class Line extends JPanel {
		private static final long serialVersionUID = 4811928244156158969L;

		public Line(Player player) throws NullPointerException{
            setLayout(new GridBagLayout());


            GridBagConstraints gbc = new GridBagConstraints();
            for (int row = 0; row < 5; row++) {
                for (int col = 0; col < 5; col++) {
                    Color color = (!player.getLines().getCase()[row][col].isEmpty()?player.getLines().getCase()[row][col].getContent().getType().getColor().getColor():new Color(0xBB9B75));
                    CellL cell = new CellL(color);
                    cell.setName(String.valueOf(row));
                    if(player == Game.getPlayers()[MouseEvents.turn] && Game.getPlayers()[MouseEvents.turn].handStock!=null) {
                        cell.addMouseListener(MouseEvents.receiveLine);
                    }
                    if(col > row)cell.setVisible(false);
                    gbc.gridx = col;
                    gbc.gridy = row;
                    add(cell, gbc);
                }
            }
        }

    }

    public static class Wall extends JPanel {
		private static final long serialVersionUID = -283605613157135600L;

		public Wall(Player player) throws NullPointerException{
            setLayout(new GridBagLayout());


            GridBagConstraints gbc = new GridBagConstraints();
            for (int row = 0; row < 5; row++) {
                for (int col = 0; col < 5; col++) {
                    Color color = player.getWall().getCase()[row][col].getContent()!=null?player.getWall().getCase()[row][col].getRestriction().getArr().get(0).getColor().getColor(): datamodel.src.Color.makeTransparent(player.getWall().getCase()[row][col].getRestriction().getArr().get(0).getColor().getColor(), 25);
                    gbc.gridx = col;
                    gbc.gridy = row;
                    add(new Cell(color), gbc);
                }
            }
        }
    }

    public static class Floor extends JPanel {
		private static final long serialVersionUID = -6517990379640934996L;

		public Floor(Player player) throws NullPointerException{
            setBounds(10,10,10,10);
            setPreferredSize(new Dimension(250, 250));
            this.setBackground(Color.lightGray);


            GridBagConstraints gbc = new GridBagConstraints();
                for (int col = 0; col < 7; col++) {
                    Color color;
                    try {
                        color = player.getFloor().getCase()[col].getContent().getType().getColor().getColor();
                    }catch (java.lang.NullPointerException e){
                        color = new Color(0xBB9B75);
                    }
                    gbc.gridx = col;
                    gbc.gridy = col;
                    CellF cell = new CellF(color);
                    cell.setName("floor");
                    if(player == Game.getPlayers()[MouseEvents.turn] && Game.getPlayers()[MouseEvents.turn].handStock!=null) {
                        cell.addMouseListener(MouseEvents.receiveFloor);
                    }
                    if(col==0) cell.setText("-1");
                    else if(col==1) cell.setText("-1");
                    else if(col==2) cell.setText("-2");
                    else if(col==3) cell.setText("-2");
                    else if(col==4) cell.setText("-2");
                    else if(col==5) cell.setText("-3");
                    else if(col==6) cell.setText("-3");
                    cell.setForeground(Color.lightGray);
                    cell.setFont(new Font("Montserrat", Font.BOLD, 9));
                    add(cell, gbc);
                }

        }
    }


    static class Cell extends JButton {
		private static final long serialVersionUID = 797877739379443931L;

		public Cell(Color background) {

            setContentAreaFilled(false);
            setBorderPainted(false);
            setBackground(background);
            setOpaque(true);
        }

        @Override
        public Dimension getPreferredSize() {
            return new Dimension(13, 13);
        }

    }
    static class CellL extends JButton {
		private static final long serialVersionUID = -2681342471139466649L;

		public CellL(Color background) {
            setContentAreaFilled(false);
            setBorderPainted(false);
            setBackground(background);
            setOpaque(true);
        }

        @Override
        public Dimension getPreferredSize() {
            return new Dimension(13, 13);
        }

    }
    static class CellF extends JLabel {
		private static final long serialVersionUID = 4818372667539472058L;

		public CellF(Color background) {
            setBackground(background);
            setOpaque(true);
            setForeground(Color.WHITE);
            setText("");
            setPreferredSize(new Dimension(20,20));
        }

        @Override
        public Dimension getPreferredSize() {
            return new Dimension(15, 15);
        }

    }

    public static class Center extends JPanel {
		private static final long serialVersionUID = 7034126329130513155L;

		public Center() throws NullPointerException{

            this.setBackground(new Color(0x503538));
            //mainContainer.add(this, BorderLayout.CENTER);

            for(int i = 0 ; i < Game.getShared().getCenter().getTiles().size(); i++) {
                JLabel jLabC = new JLabel("");
                jLabC.setOpaque(true);
                jLabC.setBackground(Game.getShared().getCenter().getTiles().get(i).getType().getColor().getColor());
                jLabC.setBorder(new EmptyBorder(10, 10, 10, 10));
                jLabC.setBounds(20,20,20,20);
                jLabC.setTransferHandler(new TransferHandler("text"));
                EventQueue.invokeLater( () ->
                {
                    if(Game.getPlayers()[MouseEvents.turn].handStock == null) jLabC.addMouseListener(MouseEvents.clickedCenter);
                });
                this.add(jLabC);
            }
        }
    }

    public static void centerMaj() {
        centerPanel = new Center();
    }

    public static class Fabric extends JPanel {
		private static final long serialVersionUID = 2322335297255342964L;

		public Fabric() throws NullPointerException{
            datamodel.src.Fabric[] fabrics = Game.getShared().getFabrics();

            for (int i = 0; i < Game.getShared().getFabrics().length; i++) {
                JPanel fabric = new JPanel();
                fabric.setLayout(new GridLayout(2, 2));
                fabric.setBorder(BorderFactory.createLineBorder(Color.BLACK));
                fabric.setBackground(Color.lightGray);
                fabric.setName(String.valueOf(i));
                fabric.setBounds(i, 0, 0, 0);
                for (int j = 0; j < 4; j++) {
                    JLabel jLab = new JLabel("");
                    jLab.setOpaque(true);
                    try {
                        jLab.setBackground((fabrics[i].getTiles()[j].getType().getColor().getColor())!=null?fabrics[i].getTiles()[j].getType().getColor().getColor(): Color.gray);
                    }catch (java.lang.NullPointerException e) {
                        jLab.setBackground(Color.lightGray);
                    }
                    jLab.setBorder(new EmptyBorder(10, 10, 10, 10));
                    jLab.setBounds(20,20,20,20);
                    jLab.setTransferHandler(new TransferHandler("text"));
                    if(Game.getPlayers()[MouseEvents.turn].handStock == null) jLab.addMouseListener(MouseEvents.clickedFabrics);
                    fabric.add(jLab);
                }
                fabric.setBorder(new EmptyBorder(70, 70, 70, 70));


                this.add(fabric);
            }
        }
    }

    public static void fabricsMaj() {
        centerPanel = new Center();
    }


    public static class Hand extends JPanel {
		private static final long serialVersionUID = 5607600269662187114L;
		
		public Hand(Player player) throws NullPointerException {
            setBounds(10, 10, 10, 10);
            setPreferredSize(new Dimension(250, 250));
            this.setLayout(new GridLayout(2, 0));
            this.setBackground(Color.lightGray);


            GridBagConstraints gbc = new GridBagConstraints();
            JLabel hand = new JLabel("Hand");
            hand.setFont(new Font("Montserrat", Font.PLAIN, 18));
            this.add(hand);
            JPanel handComponent = new JPanel();
            handComponent.setBackground(Color.lightGray);
            this.setLayout(new FlowLayout());
            this.add(handComponent);
                if(player == Game.getPlayers()[MouseEvents.turn]) {
                    try{
                        for (int i = 0; i < Game.getPlayers()[MouseEvents.turn].handStock.size(); i++) {
                            Color color;
                            try {
                                color = Game.getPlayers()[MouseEvents.turn].handStock.get(i).getType().getColor().getColor();
                                CellH cell = new CellH(color);
                                handComponent.add(cell, gbc);
                            } catch (java.lang.NullPointerException e) {
                                System.out.println("error " + e);
                            }
                        }
                    }catch (java.lang.NullPointerException ignored) {
                    }
            }



        }
        static class CellH extends JLabel {
			private static final long serialVersionUID = -6664054754870150676L;

			public CellH(Color background) {
                setBackground(background);
                setOpaque(true);
            }

            @Override
            public Dimension getPreferredSize() {
                return new Dimension(15, 15);
            }

        }
    }


    public static void gameEnd() {
        JOptionPane.showMessageDialog(mainContainer.getParent(), "Congratulations " + Azul.g1.getWinner().getName() + " won the game with a score of " +Azul.g1.getWinner().getScore());
    }

    public static void gameViewMaj() {
        gameView.dispose();
        new GameView();
    }

}
