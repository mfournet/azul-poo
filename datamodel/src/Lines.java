package datamodel.src;

import java.util.LinkedList;
import java.util.List;

import datamodel.src.Case.UnableToPutTileInCase;
import datamodel.src.Floor.FloorIsFull;

public class Lines {
	private Case[][] cases = new Case[5][5];
	private Player player;

	public Lines(Player p) {
		for (int i = 0; i < cases.length; i++) {
			for (int j = 0; j < cases[i].length; j++) {
				cases[i][j] = new Case(new TRest().modul("Colors"));
			}
		}
		player = p;
	}

	public Case[][] getCase() {
		return cases;
	}
	
	/**
	 * Function used to display the lines. Only the usable spaces are used.
	 */
	public void display() {
		for (int j = 1; j <= cases.length; j++) {
			for (int i = 1; i <= cases.length - j; i++) {
				System.out.print("    ");
			}

			for (int k = 1; k <= j; k++) {
				cases[j - 1][k - 1].display();
			}

			System.out.println();
		}
	}

	/**
	 * Put an array of tiles in a specified line, and if there are too much put the
	 * rest in the floor, and if still too much, put it in the discard
	 * 
	 * @param t is the array of tiles to be put
	 * @param n  is the specified line to put tiles int, 5 is for the floor
	 * @return if the player just completed a line of his wall, which means the end
	 *         of the game
	 * @throws UnableToPutTileInCase could be thrown if we're trying to put tiles
	 *                               which type is different from the ones already
	 *                               in the line
	 * @throws WrongColor            when adding another color in a line that
	 *                               already got a specific one
	 */
	public boolean put(LinkedList<Tile> t, int n) throws UnableToPutTileInCase, WrongSelectionException {
		if (t.size() == 0)
			throw new WrongSelectionException();
		
		if (n == 5) {
			putDump(t, 0);
			return false;
		}

		int point = 0;
		int fp = 0;

		if (t.get(0).getType() == TType.FirstPlayer) {
			LinkedList<Tile> tmp = new LinkedList<Tile>();
			tmp.add(t.get(0));
			putDump(tmp, 0);
			fp++;
		}

		int limit = n > t.size() - fp - 1 ? t.size() - fp - 1 : n;
		int shift = 0;

		for (int i = 0; i <= limit; i++) {
			if (cases[n][i].isEmpty()) {
				if (i == 0) {
					if(player.getWall().isColorInLine(t.get(fp).getType(), n)) {
						throw new WrongSelectionException();
					}
				}
				if (i != 0) {
					if (t.get(i + fp + shift).getType() != cases[n][0].getContent().getType()) {
						System.out.println("Wrong color !");
						throw new WrongSelectionException();
					}
				}
				cases[n][i].put(t.get(i + fp + shift));
				point++;
			} else {
				shift--;
				limit = n > limit + 1 ? limit + 1 : n;
			}
		}

		putDump(t, point + fp);
		return completedWall(n);
	}

	/**
	 * Function used to put tiles in the "dump" section, which is the floor or the discard
	 * @param t are the tiles that are to be put to dump
	 * @param point is the position from where the tiles are to be put to dump
	 * @throws UnableToPutTileInCase when floor.put encountered a problem
	 */
	public void putDump(LinkedList<Tile> t, int point) throws UnableToPutTileInCase {
		try { // What we couldn't put in the line, we put in the floor
			List<Tile> tmp = t.subList(point, t.size());
			player.getFloor().put(tmp);
		} catch (FloorIsFull e) { // What we couldn't put in the floor, we put in the discard
			List<Tile> tmp = t.subList( e.getIndex(), t.size());
			Game.discard.put(tmp);
		}
	}

	/**
	 * Check if the game will be ended because of a full wall
	 * @param line is the line to check for "fullness"
	 * @return true if the wall's line will be full
	 */
	private boolean completedWall(int line) {
		if (!lineIsFull(line))
			return false;
		if (!player.getWall().wallLineAlmostFull(line))
			return false;
		return true;
	}

	/**
	 * Simplye check if the line is full
	 * @param line is the line to be checked
	 * @return true if line is full
	 */
	private boolean lineIsFull(int line) {
		for (int i = 0; i <= line; i++) {
			if (cases[line][i].getContent() == null)
				return false;
		}
		return true;
	}

	/**
	 * Will put tiles of the full lines in wall
	 * @return the score gained from this action
	 * @throws UnableToPutTileInCase if a problem was encountered
	 */
	public int buildWall() throws UnableToPutTileInCase {
		int score = 0;
		for (int i = 0; i < 5; i++) {
			if (lineIsFull(i)) {
				if (Azul.mode == 1) {
					System.out.println("You're building the line " + (i+1) + " of your wall.");
				}
				TType tt = getLineColor(i);
				score += player.getWall().buildWall(i, tt);
				emptyLine(i);
			}
		}
		return score;
	}

	/**
	 * Check which color is in the line
	 * @param i is the line to be checked
	 * @return the type in the line
	 */
	private TType getLineColor(int i) {
		for (int j = 0; j <= i; j++) {
			if (cases[i][j].getContent().getType() != TType.Joker)
				return cases[i][j].getContent().getType();
		}
		return TType.Joker;
	}

	/**
	 * Make the line completely empty
	 * @param i is the line to be emptied
	 */
	private void emptyLine(int i) {
		for (int j = 0; j < cases[i].length; j++) {
			if (j != cases[i].length - 1) // Because the last one goes in to the wall
				Game.discard.put(cases[i][j].getContent());
			cases[i][j] = new Case(new TRest().modul("Colors"));
		}
	}

	public static class WrongSelectionException extends Exception {
		private static final long serialVersionUID = 5328385797198797162L;

	}
}
