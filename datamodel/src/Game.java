package datamodel.src;

import java.util.Scanner;

import datamodel.src.Case.UnableToPutTileInCase;
import view.src.GameView;
import java.util.Random;

public class Game {
	static private Player[] players;
	static private Shared shared;
	static public int turn;
	static public Scanner sc= new Scanner(System.in);

	public static Bag getBag() {
		return bag;
	}
	public static Player[] getPlayers() {
		return players;
	}


	public static Shared getShared() {
		return shared;
	}

	private static Bag bag;
	static public Bag discard;


	// Include params from Azul here
	public Game(Player[] playersArray) {
		players = playersArray;
		bag = new Bag();
		bag.fill();
		discard = new Bag();
		shared = new Shared(bag);
	}

	public void displayGame() {
		shared.displayShared();
		for(int i = 0; i < players.length; i++) {
			players[i].display();
			System.out.println();
		}
		System.out.println();
	}

	public void play() {
		Random r = new Random();
		turn = r.nextInt(players.length);
		int turns = 1;
		boolean playerEnded = false;
		while (!playerEnded) {
			while (!isEmpty()) {
				try {
					System.out.println("====================================================\n"
							         + "===================== TURN "+turns+" =======================\n"
							         + "====================================================\n");
					displayGame();
					if (players[turn].play()) {
						playerEnded = true;
					}
					turn = (turn + 1) % players.length;
					turns++;
					shared.cleanAll();
				} catch (UnableToPutTileInCase e) {
					System.out.print("There was a problem during your turn, the game can't proceed.");
					e.printStackTrace();
					System.exit(1);
				} catch (Lines.WrongSelectionException e) {
					System.out.println("Please play again.");
				} catch (Player.EmptyLocationException e) {
					System.out.println("Please play again.");
				}
			}
			try {
				buildWall();

			} catch (UnableToPutTileInCase e) {
				System.out.print("There was a problem during your turn, the game can't proceed.");
				e.printStackTrace();
				System.exit(1);
			}
			turn = endOfTurn();
			fill();
		}
		displayGame();
		if(isGameEnded()) gameEnd(1);
		//if(bag.isEmpty() && discard.isEmpty()) gameEnd(0);
	}

	public void playGui() {
		new GameView();
		System.out.println("playGUI");
	}

	public void fill() {
		if (bag.isEmpty()) bag.put(discard.getTiles());
		discard.empty();
		shared.fill();
	}

	public boolean isEmpty() {
		return shared.isEmpty();
	}
	
	public void buildWall() throws UnableToPutTileInCase {
		for (Player p : players) {
			if (Azul.mode == 1) {
				System.out.println("It's " + p.getName() + "'s turn to build his wall.");
			}
			p.buildWall();
		}
	}
	
	public int endOfTurn() {
		for (Player p : players) {
			p.endOfTurn();
		}
		for (int i = 0; i < players.length; i++) {
			if (players[i].isFirst()) return i;
		}
		return 0;
	}

	public boolean isGameEnded() {
		for (Player p : players) {
			if(p.playerEnded()) return true;
		}
		return false;
	}

	public void gameEnd(int gui) { //0 = terminal 1 = GUI
		if(gui==0) GameView.gameEnd();
		else {
			System.out.println(
					"=======================END===================\n" +
					"Congratulations " + getWinner().getName() + " won the game with +" +getWinner().getScore() +
					"=======================END===================\n"
			);
		}
	}

	public Player getWinner() {
		Player winner = players[0];
		for (Player player : players) {
			if (winner.getScore() < player.getScore()) winner = player;
		}
		return winner;
	}
}
