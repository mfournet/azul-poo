package datamodel.src;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class Bag {
	private LinkedList<Tile> tiles;
	
	public Bag() {
		tiles = new LinkedList<Tile>();
	}


	public LinkedList<Tile> getTiles() {
		return tiles;
	}

	public void fill() {
		for(int i = 0; i < 20; i++) {
			tiles.add(new Tile(TType.Red));
		}
		for(int i = 20; i < 40; i++) {
			tiles.add(new Tile(TType.Yellow));
		}
		for(int i = 40; i < 60; i++) {
			tiles.add(new Tile(TType.Blue));
		}
		for(int i = 60; i < 80; i++) {
			tiles.add(new Tile(TType.White));
		}
		for(int i = 80; i < 100; i++) {
			tiles.add(new Tile(TType.Black));
		}
		Collections.shuffle(tiles);
	}

	public Tile getTileFirst() {
		Tile t = tiles.getFirst();
		tiles.removeFirst();
		return t;
	}
	public void display() {
		for(int i = 0; i < tiles.size(); i++){
			tiles.get(i).display();
		}
	}

	public void put(List<Tile> linkedList) {
		for (Tile t : linkedList) {
			tiles.add(t);
		}
	}
	
	public void put(Tile t) {
		tiles.add(t);
	}


	public boolean isEmpty() {
		return tiles.isEmpty();
	}


	public void empty() {
		tiles = new LinkedList<Tile>();
	}
}
