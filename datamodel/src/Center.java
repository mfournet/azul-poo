package datamodel.src;
import java.util.ArrayList;
import java.util.LinkedList;

import datamodel.src.Case.UnableToPutTileInCase;

public class Center {
	private ArrayList<Tile> tiles;
	
	public Center() {
		tiles = new ArrayList<Tile>();
		tiles.add(new Tile(TType.FirstPlayer));
	}

	public ArrayList<Tile> getTiles() {
		return tiles;
	}

	public void display() {
		if(!tiles.isEmpty()){
			for (Tile tile : tiles) {
				tile.display();
			}
		}
	}
	
	public void put(Tile t) {
		this.tiles.add(t);
	}
	
	public LinkedList<Tile> take(TType ttype) throws UnableToPutTileInCase {
		LinkedList<Tile> ret = new LinkedList<Tile>();
		for (Tile t : this.tiles) {
			if (t.getType() == ttype || t.getType() == TType.Joker || t.getType() == TType.FirstPlayer) {
				ret.add(t);
				prepRem(t);
			}
		}
		return ret;
	}

	public boolean isEmpty() {
		return this.tiles.isEmpty();
	}

	private LinkedList<Tile> prepPut = new LinkedList<Tile>();
	public void prepPut(Tile t) {
		prepPut.add(t);
	}
	public void finPut() {
		for (Tile t : prepPut) {
			put(t);
		}
		prepPut = new LinkedList<Tile>();
	}
	
	private LinkedList<Tile> prepRem = new LinkedList<Tile>();
	public void prepRem(Tile t) {
		prepRem.add(t);
	}
	public void finRem() {
		for (Tile t : prepRem) {
			tiles.remove(t);
		}
		prepRem = new LinkedList<Tile>();
	}
}
