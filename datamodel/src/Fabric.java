package datamodel.src;

import java.util.LinkedList;

public class Fabric {
	private Tile[] tiles;
	private Bag bag;

	public Fabric(Bag bag) {
		tiles = new Tile[4];
		this.bag = bag;
		for (int i = 0; i < tiles.length; i++) {
			/*
			 * tiles[i] = Game.bag.getTiles().getFirst();
			 * Game.bag.getTiles().removeFirst();
			 */
			tiles[i] = bag.getTiles().pop();
		}
	}

	public void fill() {
		for (int i = 0; i < tiles.length; i++) {
			/*
			 * tiles[i] = Game.bag.getTiles().getFirst();
			 * Game.bag.getTiles().removeFirst();
			 */
			tiles[i] = bag.getTiles().pop();
		}
	}

	public Tile[] getTiles() {
		return tiles;
	}

	public void display1() {
		for (int i = 0; i < tiles.length / 2; i++) {
			if (tiles[i] != null) {
				tiles[i].display();
			} else {
				Color.getColorNull();
			}
		}
	}

	public void display2() {

		for (int j = (tiles.length / 2); j < tiles.length; j++) {
			if (tiles[j] != null) {
				tiles[j].display();
			} else {
				Color.getColorNull();
			}
		}
	}

	public void display() {
		display1();
		System.out.println();
		display2();
		System.out.println();
	}

	public LinkedList<Tile> take(TType ttype) {
		LinkedList<Tile> ret = new LinkedList<Tile>();
		for (Tile t : this.tiles) {
			if (t.getType() == ttype || t.getType() == TType.Joker) {
				ret.add(t);
			} else {
				Game.getShared().getCenter().prepPut(t);
			}
		}
		if (ret.size() == 0)
			return null;
		toClean = true;
		return ret;
	}

	public boolean isEmpty() {
		for (Tile t : tiles) {
			if (t != null) {
				return false;
			}
		}
		return true;
	}

	private boolean toClean = false;
	public void clean() {
		if (toClean) {
			tiles = new Tile[4];
			toClean = false;
		}
	}
}
