package datamodel.src;

public enum Color {
    RESET("\033[0m", null),

    // Regular Colors
    BLACK("\033[0;30m", java.awt.Color.BLACK),    // BLACK
    RED("\033[0;31m", java.awt.Color.RED),      // RED
    GREEN("\033[0;32m", java.awt.Color.GREEN),    // GREEN
    YELLOW("\033[0;33m", java.awt.Color.YELLOW),   // YELLOW
    BLUE("\033[0;34m", java.awt.Color.BLUE),     // BLUE
    MAGENTA("\033[0;35m", java.awt.Color.MAGENTA),  // MAGENTA
    CYAN("\033[0;36m", java.awt.Color.CYAN),     // CYAN
    WHITE("\033[0;37m", java.awt.Color.WHITE),    // WHITE

    // Bold
    BLACK_BOLD("\033[1;30m", java.awt.Color.BLACK),   // BLACK
    RED_BOLD("\033[1;31m", java.awt.Color.RED),     // RED
    GREEN_BOLD("\033[1;32m", java.awt.Color.GREEN),   // GREEN
    YELLOW_BOLD("\033[1;33m", java.awt.Color.YELLOW),  // YELLOW
    BLUE_BOLD("\033[1;34m", java.awt.Color.BLUE),    // BLUE
    MAGENTA_BOLD("\033[1;35m", java.awt.Color.MAGENTA), // MAGENTA
    CYAN_BOLD("\033[1;36m", java.awt.Color.CYAN),    // CYAN
    WHITE_BOLD("\033[1;37m", java.awt.Color.WHITE),   // WHITE

    // Underline
    BLACK_UNDERLINED("\033[4;30m", java.awt.Color.BLACK),     // BLACK
    RED_UNDERLINED("\033[4;31m", java.awt.Color.RED),       // RED
    GREEN_UNDERLINED("\033[4;32m", java.awt.Color.GREEN),     // GREEN
    YELLOW_UNDERLINED("\033[4;33m", java.awt.Color.YELLOW),    // YELLOW
    BLUE_UNDERLINED("\033[4;34m", java.awt.Color.BLUE),      // BLUE
    MAGENTA_UNDERLINED("\033[4;35m", java.awt.Color.MAGENTA),   // MAGENTA
    CYAN_UNDERLINED("\033[4;36m", java.awt.Color.CYAN),      // CYAN
    WHITE_UNDERLINED("\033[4;37m", java.awt.Color.WHITE),     // WHITE

    // Background
    BLACK_BACKGROUND("\033[40m", java.awt.Color.BLACK),   // BLACK
    RED_BACKGROUND("\033[41m", java.awt.Color.RED),     // RED
    GREEN_BACKGROUND("\033[42m", java.awt.Color.GREEN),   // GREEN
    YELLOW_BACKGROUND("\033[43m", java.awt.Color.YELLOW),  // YELLOW
    BLUE_BACKGROUND("\033[44m", java.awt.Color.BLUE),    // BLUE
    MAGENTA_BACKGROUND("\033[45m", java.awt.Color.MAGENTA), // MAGENTA
    CYAN_BACKGROUND("\033[46m", java.awt.Color.CYAN),    // CYAN
    WHITE_BACKGROUND("\033[47m", java.awt.Color.WHITE),   // WHITE

    // High Intensity
    BLACK_BRIGHT("\033[0;90m", java.awt.Color.BLACK),     // BLACK
    RED_BRIGHT("\033[0;91m", java.awt.Color.RED),       // RED
    GREEN_BRIGHT("\033[0;92m", java.awt.Color.GREEN),     // GREEN
    YELLOW_BRIGHT("\033[0;93m", java.awt.Color.YELLOW),    // YELLOW
    BLUE_BRIGHT("\033[0;94m", java.awt.Color.BLUE),      // BLUE
    MAGENTA_BRIGHT("\033[0;95m", java.awt.Color.MAGENTA),   // MAGENTA
    CYAN_BRIGHT("\033[0;96m", java.awt.Color.CYAN),      // CYAN
    WHITE_BRIGHT("\033[0;97m", java.awt.Color.WHITE),     // WHITE

    // Bold High Intensity
    BLACK_BOLD_BRIGHT("\033[1;90m", java.awt.Color.BLACK),    // BLACK
    RED_BOLD_BRIGHT("\033[1;91m", java.awt.Color.RED),      // RED
    GREEN_BOLD_BRIGHT("\033[1;92m", java.awt.Color.GREEN),    // GREEN
    YELLOW_BOLD_BRIGHT("\033[1;93m", java.awt.Color.YELLOW),   // YELLOW
    BLUE_BOLD_BRIGHT("\033[1;94m", java.awt.Color.BLUE),     // BLUE
    MAGENTA_BOLD_BRIGHT("\033[1;95m", java.awt.Color.MAGENTA),  // MAGENTA
    CYAN_BOLD_BRIGHT("\033[1;96m", java.awt.Color.CYAN),     // CYAN
    WHITE_BOLD_BRIGHT("\033[1;97m", java.awt.Color.WHITE),    // WHITE

    // High Intensity backgrounds
    BLACK_BACKGROUND_BRIGHT("\033[0;100m", java.awt.Color.BLACK),     // BLACK
    RED_BACKGROUND_BRIGHT("\033[0;101m", java.awt.Color.RED),       // RED
    GREEN_BACKGROUND_BRIGHT("\033[0;102m", java.awt.Color.GREEN),     // GREEN
    YELLOW_BACKGROUND_BRIGHT("\033[0;103m", java.awt.Color.YELLOW),    // YELLOW
    BLUE_BACKGROUND_BRIGHT("\033[0;104m", java.awt.Color.BLUE),      // BLUE
    MAGENTA_BACKGROUND_BRIGHT("\033[0;105m", java.awt.Color.MAGENTA),   // MAGENTA
    CYAN_BACKGROUND_BRIGHT("\033[0;106m", java.awt.Color.CYAN),      // CYAN
    WHITE_BACKGROUND_BRIGHT("\033[0;107m", java.awt.Color.WHITE);     // WHITE

    private final String code;
    private final java.awt.Color color;


    Color(String code, java.awt.Color color) {
        this.code = code;
        this.color = color;
    }

    @Override
    public String toString() {
        return code;
    }

    public java.awt.Color getColor() {
        return this.color;
    }

    public static void getColorNull() {
        System.out.print(Color.CYAN); System.out.print(Color.RESET);
    }

    public static java.awt.Color makeTransparent(java.awt.Color source, int alpha)
    {
        return new java.awt.Color(source.getRed(), source.getGreen(), source.getBlue(), alpha);
    }

}
