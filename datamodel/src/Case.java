package datamodel.src;

public class Case {
	private Tile content;
	private TRest restriction;

	public Case() {
		content = null;
		restriction = new TRest().modul("All");
	}

	public Case(TRest r) {
		content = null;
		restriction = r;
	}

	public Case(Tile t, TRest r) {
		restriction = r;
		content = t;
	}

	public Tile getContent() {
		return content;
	}

	public boolean isEmpty() {
		return content == null;
	}

	public TRest getRestriction() {
		return restriction;
	}

	public void setRestriction(TRest r) {
		restriction = r;
	}

	public void display() {
		if (content != null) {
			content.display();

		} else {
			if (!(restriction.getArr().size() > 2)) {
				System.out.print(restriction.getArr().get(0).getColor());
				System.out.print(Color.BLACK_BOLD);
				System.out.print("   ");
				System.out.print(Color.RESET);
				System.out.print(" ");
			} else {
				System.out.print(Color.WHITE_BACKGROUND);
				System.out.print(Color.BLACK_BOLD);
				System.out.print("   ");
				System.out.print(Color.RESET);
				System.out.print(" ");
			}
		}
	}

	public void display(int i) {
		if (content != null) {
			System.out.print(content.getType().getColor());
			System.out.print("" + i + " ");
			System.out.print(Color.RESET);
			System.out.print(" ");
		} else {
			System.out.print(Color.BLACK_BACKGROUND);
			System.out.print("" + i + " ");
			System.out.print(Color.RESET);
			System.out.print(" ");
		}
	}

	public Color getColor() {
		return content.getType().getColor();
	}

	/**
	 * @param t is the tile we want to put in our content
	 * @throws UnableToPutTileInCase if the TRest is not ok with t.type
	 */
	public void put(Tile t) throws UnableToPutTileInCase {
		if (restriction.onlyNull())
			restriction = new TRest().modul("All");
		for (TType type : restriction.getArr()) {
			if (type == t.getType()) {
				content = t;
				return;
			}
		}
		throw new UnableToPutTileInCase(t);
	}
	
	public static class UnableToPutTileInCase extends Exception {
		private static final long serialVersionUID = 3156046631547689422L;
		public Tile t;

		public UnableToPutTileInCase(Tile tile) {
			t = tile;
		}

		public UnableToPutTileInCase() {
			t = null;
		}
	}
}
