package datamodel.src;

import datamodel.src.Case.UnableToPutTileInCase;

public class Wall {
	private Case[][] cases;

	/**
	 * The initialisation depends on the mode of Azul
	 */
	public Wall() {
		cases = new Case[5][5];
		if (Azul.mode == 0)
			init0();
		else
			init1();
	}

	private void init1() {
		for (int i = 0; i < cases.length; i++) {
			for (int j = 0; j < cases[i].length; j++) {
				cases[i][j] = new Case(new TRest().modul("Colors"));
			}
		}
	}

	private void init0() {
		for (int i = 0; i < cases.length; i++) {
			for (int j = 0; j < cases[i].length; j++) {
				if (i  == j + 1 || j  == i + 4)
					cases[i][j] = new Case(new TRest().modul("Yellow"));
				if (i == j + 2 || j == i + 3)
					cases[i][j] = new Case(new TRest().modul("Red"));
				if (i == j)
					cases[i][j] = new Case(new TRest().modul("Blue"));
				if (i == j + 3 || j == i + 2)
					cases[i][j] = new Case(new TRest().modul("Black"));
				if (i == j + 4 || j == i + 1)
					cases[i][j] = new Case(new TRest().modul("White"));
			}
		}
	}

	public Case[][] getCase() {
		return cases;
	}

	public void display() {
		for (int i = 0; i < cases.length; i++) {
			for (int j = 0; j < cases[i].length; j++) {
				cases[i][j].display();
			}
			System.out.println();
		}
	}

	/**
	 * Function to complete a specified line of the wall with a specified color
	 * 
	 * @param t is the color we want to add
	 * @param l is the line we want to complete
	 * @throws UnableToPutTileInCase when, in mode 1, you try to add a tile where
	 *                               you can't
	 */
	public void put(TType t, int l) throws UnableToPutTileInCase {
		int c = (Azul.mode == 0) ? -1 : Game.sc.nextInt() - 1; // If we're in mode 1 we have to chose the column
		// Mode 1 part
		if (c != -1) {
			cases[l][c].put(new Tile(t));
			return;
		}
		// Mode 0 part
		for (int i = 0; i < 5; i++) {
			if (cases[l][i].getRestriction().getArr().contains(t)) {
				try {
					cases[l][i].put(new Tile(t));
				} catch (UnableToPutTileInCase e) {
					System.out.println("This error should not happen");
					e.printStackTrace();
					System.exit(1);
				}
			}
		}
	}

	/**
	 * Check if there is only one space left in the line (or less because why not)
	 * @param wall's line to be checked
	 * @return a simple boolean to answer the first question
	 */
	public boolean wallLineAlmostFull(int line) {
		int compt = 0;
		for (int i = 0; i <= 4; i++) {
			if (cases[line][i].getContent() == null)
				compt++;
		}
		return (compt <= 1);
	}

	/**
	 * Put a tile from a line to the corresponding line of the wall
	 * @param i is the line where to put the tile
	 * @param tt is the type of the tile to be put
	 * @return the score assigned to the move
	 * @throws UnableToPutTileInCase
	 */
	public int buildWall(int i, TType tt) throws UnableToPutTileInCase {
		int pos = getBuildPos(i, tt);
		cases[i][pos].put(new Tile(tt));
		return score(i, pos);
	}

	/**
	 * Function that return score gained from the placing
	 * @param y first coordinate of the tile placed
	 * @param x second coordinate of the tile placed
	 * @return the score
	 */
	private int score(int y, int x) {
		int score = 1;
		if ((x > 0 && !cases[y][x - 1].isEmpty()) || (x < 4 && !cases[y][x + 1].isEmpty())) {
			for (int i = 0; i < 5; i++) {
				if (!cases[y][i].isEmpty())
					score++;
			}
		}
		if ((y > 0 && !cases[y - 1][x].isEmpty()) || (y < 4 && !cases[y + 1][x].isEmpty())) {
			for (int i = 0; i < 5; i++) {
				if (!cases[i][x].isEmpty())
					score++;
			}
		}
		return score;
	}

	/**
	 * Get the position of where to put a tile when building the wall
	 * @param i the line with which we're working
	 * @param tt the type of the tile to be put
	 * @return the position (only the second coord 'cause we already got the first with i )
	 */
	private int getBuildPos(int i, TType tt) {
		int pos = -1;
		if (Azul.mode == 1) {
			boolean flag = true;
			while (pos < 0 || pos > 4 || flag) {
				System.out.println("Select in which row you want to place the tile, from 1 to 5.");
				pos = Game.sc.nextInt() - 1;
				if (pos < 0 || pos > 4) {
					System.out.println("Your can't chose this.");
				} else {
					flag = !getPosValid(i, pos, tt);
				}
			}
		} else {
			for (int j = 0; j < 5; j++) {
				if (cases[i][j].getRestriction().getArr().contains(tt)) {
					pos = j;
				}
			}
		}
		return pos;
	}
	
	/**
	 * Security funciton to check that the position is correct
	 * @param i first coord of the position
	 * @param pos second coord of the position
	 * @param tt type of tile to be put
	 * @return validity boolean
	 */
	public boolean getPosValid(int i, int pos, TType tt) {
		for (int j = 0; j < 5; j++) {
			if(!cases[i][j].isEmpty()) {
				if (cases[i][j].getContent().getType() == tt) {
					return false;
				}
			}
		}
		for (int j = 0; j < 5; j++) {
			if(!cases[j][pos].isEmpty()) {
				if (cases[j][pos].getContent().getType() == tt) {
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * Check validity for a position, but only in a line
	 * @param type of the tile
	 * @param n line to be checked
	 * @return boolean
	 */
	public boolean isColorInLine(TType type, int n) {
		for (int i = 0; i < 5; i++) {
			if (!cases[n][i].isEmpty()) {
				if (cases[n][i].getContent().getType() == type) {
					return true;
				}
			}
		}
		return false;
	}

	public boolean oneLineFull() {
		for (Case[] aCase : cases) {
			int full = 0;
			for (int j = 0; j < aCase.length; j++) {
				if (!aCase[j].isEmpty()) full++;
				if (full == 5) return true;
			}
		}
		return false;
	}
}
