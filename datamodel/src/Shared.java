package datamodel.src;

public class Shared {
	private Fabric[] fabrics;
	private Center center;
	private Bag bag;

	public Shared(Bag bag) {
		this.bag = bag;
		center = new Center();
		fabrics = new Fabric[(Player.id * 2) + 1];
		for (int i = 0; i < fabrics.length; i++) {
			fabrics[i] = new Fabric(bag);
		}
	}

	public Fabric[] getFabrics() {
		return fabrics;
	}

	public Center getCenter() {
		return center;
	}

	public void displayShared() {
		for (int i = 0; i < fabrics.length; i++) {
			fabrics[i].display1();
			System.out.print("	");
		}
		System.out.println();
		for (int i = 0; i < fabrics.length; i++) {
			fabrics[i].display2();
			System.out.print("	");
		}
		
		System.out.println("\n");

		System.out.print("\n");
		center.display();
		System.out.print("\n\n");
	}

	public boolean isEmpty() {
		boolean flag = true;
		for (Fabric f : fabrics) {
			if (!f.isEmpty())
				flag = false;
		}
		if (flag) {
			if (!center.isEmpty())
				flag = false;
		}
		return flag;
	}

	public void fill() {
		fabrics = new Fabric[(Player.id * 2) + 1];
		for (int i = 0; i < fabrics.length; i++) {
			fabrics[i] = new Fabric(bag);
		}
		center.put(new Tile(TType.FirstPlayer));
	}

	public void cleanAll() {
		center.finPut();
		center.finRem();
		for (Fabric f : fabrics) {
			f.clean();
		}
	}
}
