package datamodel.src;

import java.util.LinkedList;
import java.util.List;

import datamodel.src.Case.UnableToPutTileInCase;

public class Floor {
	private Case[] cases;
	
	public Floor() {
		cases = new Case[7];
		for(int i = 0; i < cases.length; i++) {
			cases[i] =  new Case();
		}
	}
	
	public Case[] getCase() {
		return cases;
	}

	public void display() {
		cases[0].display(-1);
		cases[1].display(-1);
		cases[2].display(-2);
		cases[3].display(-2);
		cases[4].display(-2);
		cases[5].display(-3);
		cases[6].display(-3);
	}

	public void resetFloor() {
		cases = new Case[7];
		for(int i = 0; i < cases.length; i++) {
			cases[i] =  new Case();
		};
	}
	
	/**
	 * Put an array of tiles in our Floor
	 * @param t are the tiles put in the floor
	 * @throws UnableToPutTileInCase because of the Case.put but should not occur
	 * @throws FloorIsFull will pass the index at which we stopped putting tiles
	 */
	public void put(List<Tile> t) throws UnableToPutTileInCase, FloorIsFull {
		boolean flag;
		for (int i = 0; i < t.size() ; i++) {
			flag = true;
			for (Case c : cases) {
				if (c.isEmpty()) {
					c.put(t.get(i));
					flag = !flag;
					break;
				}
			}
			if (flag) throw new FloorIsFull(i);
		}
	}
	public void put(Tile t) throws UnableToPutTileInCase, FloorIsFull {
		LinkedList<Tile> tmp = new LinkedList<Tile>();
		tmp.add(t);
		put(tmp);
	}

	/**
	 * Exception thrown when our Floor is Full
	 * It throw the index at which we stopped putting the tiles
	 */
	public class FloorIsFull extends Exception {
		private int index;
		private static final long serialVersionUID = -2770056165319097908L;

		public int getIndex() { return index;};
		
		public FloorIsFull(int i) {
			super();
			index = i;
		}
	}

	/**
	 * Didn't find any function that would enable us to find the value more easily
	 * @return the score
	 */
	public int endOfTurn() {
		int score = 0;
		if(cases[0].isEmpty()) return score;
		score -= 1;
		if(cases[1].isEmpty()) return score;
		score -= 1;
		if(cases[2].isEmpty()) return score;
		score -= 2;
		if(cases[3].isEmpty()) return score;
		score -= 2;
		if(cases[4].isEmpty()) return score;
		score -= 2;
		if(cases[5].isEmpty()) return score;
		score -= 3;
		if(cases[6].isEmpty()) return score;
		score -= 3;
		return score;
	}
}
