package datamodel.src;

import view.src.*;

import java.util.Scanner;
public class Azul {
	public static Game g1;
	public static int mode; // 0 is classic, 1 is with uncolored wall
	static Scanner sc= new Scanner(System.in);

	
	public Azul() {

		System.out.print("GUI(0) or terminal(1) ? ");
		int nbG = getNbInRange(0,1);
		boolean gui = nbG == 0;
		if(gui) {
			new MainView();
		}else {
			System.out.print("How many players ? ");
			int nb = getNbInRange(2,4);
			System.out.print("What mode ? ");
			mode = getNbInRange(0,1);
			Player[] players = new Player[nb];
			sc.nextLine();
			for(int i = 0; i < players.length; i++) {
				System.out.println("What's the name of the player "+(i+1));
				String name = sc.nextLine();
				players[i] = new Player(name);
			}
			g1 = new Game(players);
			g1.play();
		}
	}

	public static void main(String[] args) {
		new Azul();
	}
	
	/**
	 * Be sure to get a number in the range described from the user
	 * @param x is the lower limit
	 * @param y is the upper limit
	 * @return the number gotten from user
	 */
	private static int getNbInRange(int x, int y) {
		int nb = x-1;
		while (nb < x || nb > y) {
			System.out.println("Please enter a number (" + x + "-"+ y + ")");
			nb = sc.nextInt();
		}
		return nb;
	}

	public static Player[] getPlayers() {
		return Game.getPlayers();
	}

	public static void setPlayers(Player[] players) {
		//Azul.players = players;
	}


	/*public static void initPlayers(int selectedIndex) {
		players = new Player[selectedIndex];
		System.out.println("initPL = "+selectedIndex);
		for(int i = 0; i < selectedIndex; i++) {
			players[i] = new Player("toto");
		}
	}*/
}
