package datamodel.src;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

public class TRest {
	TType Red = TType.Red;
	TType Blue = TType.Blue;
	TType Yellow = TType.Yellow;
	TType White = TType.White;
	TType Black = TType.Black;
	TType Joker = TType.Joker;
	TType FirstPlayer = TType.FirstPlayer;
	private ArrayList<TType> arr;

	public ArrayList<TType> getArr() {
		return arr;
	}

	public void All() {
		arr = new ArrayList<TType>();
		arr.add(Red);
		arr.add(Blue);
		arr.add(Yellow);
		arr.add(White);
		arr.add(Black);
		arr.add(Joker);
		arr.add(FirstPlayer);
	}

	public void Red() {
		arr = new ArrayList<TType>();
		arr.add(Red);
		arr.add(Joker);
	}

	public void Blue() {
		arr = new ArrayList<TType>();
		arr.add(Blue);
		arr.add(Joker);
	}

	public void Yellow() {
		arr = new ArrayList<TType>();
		arr.add(Yellow);
		arr.add(Joker);
	}

	public void White() {
		arr = new ArrayList<TType>();
		arr.add(White);
		arr.add(Joker);
	}

	public void Black() {
		arr = new ArrayList<TType>();
		arr.add(Black);
		arr.add(Joker);
	}

	public void Colors() {
		arr = new ArrayList<TType>();
		arr.add(Red);
		arr.add(Blue);
		arr.add(Yellow);
		arr.add(White);
		arr.add(Black);
		arr.add(Joker);
	}

	/**
	 * Help us to call a function of this class using a string
	 * 
	 * @param s is the function name
	 * @return this so we can directly call the function on a new object and use it
	 *         as a parameter
	 */
	public TRest modul(String s) {
		java.lang.reflect.Method method = null;
		try {
			method = this.getClass().getMethod(s);
		} catch (SecurityException e) {
			// This should not happen as we're calling methods from this class
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// The name given has a typo in it
			e.printStackTrace();
		}

		try {
			method.invoke(this);
		} catch (IllegalArgumentException e) {
			// There are no arguments acutally
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// We're still in the same class
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// Our methods don't throw anything so this should'nt be happening
			e.printStackTrace();
		}

		return this;
	}

	public void del(String s) {
		for (int i = 0; i < arr.size(); i++) {
			if (arr.get(i) == TType.valueOf(s))
				arr.remove(i);
		}
	}

	public void add(String s) {
		arr.add(TType.valueOf(s));
	}

	public void del(TType t) {
		for (int i = 0; i < arr.size(); i++) {
			if (arr.get(i) == t)
				arr.remove(i);
		}
	}

	public void add(TType t) {
		arr.add(t);
	}
	
	public boolean onlyNull() {
		for (TType t : arr) {
			if (t != null) {
				return false;
			}
		}
		return true;
	}
}
