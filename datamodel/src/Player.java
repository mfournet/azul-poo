package datamodel.src;

import java.util.LinkedList;

import datamodel.src.Case.UnableToPutTileInCase;
import datamodel.src.Lines.WrongSelectionException;

public class Player {
	static int id = 0;
	private Wall wall;
	private Lines lines;
	private Floor floor;
	public LinkedList<Tile> handStock = null;
	private int score;
	private String name;
	private boolean isFirst;

	public Player(String name) {
		this.name = name;
		this.score = 0;
		this.isFirst = false;

		this.wall = new Wall();
		this.lines = new Lines(this);
		this.floor = new Floor();
		id++;
	}

	public Wall getWall() {
		return wall;
	}

	public Lines getLines() {
		return lines;
	}

	public Floor getFloor() {
		return floor;
	}

	public int getScore() {
		return score;
	}

	public String getName() {
		return name;
	}

	public boolean isFirst() {
		return isFirst;
	}

	public void display() {
		System.out.print(Color.YELLOW_BACKGROUND);
		System.out.print(Color.BLACK_BOLD);
		System.out.print("	" + this.name + "		Score = " + this.score + "		");
		System.out.print(Color.RESET);
		System.out.print("\n\n");
		lines.display();
		System.out.println();
		wall.display();
		System.out.println();
		floor.display();
		System.out.println();
	}

	public boolean play() throws UnableToPutTileInCase, WrongSelectionException, EmptyLocationException {
		boolean flag = false;
		System.out.println(name + "'s turn !");
		LinkedList<Tile> t = pick();
		while (t == null) {
			System.out.println("Your can't chose this.");
			t = pick();
		}
		System.out.println();
		System.out.println("You got :");
		for (Tile tile : t) {
			tile.display();
			if (tile.getType() == TType.FirstPlayer) {
				this.isFirst = true;
				flag = true;
			}
		}
		System.out.println();
		boolean ret = false;
		try {
			ret = put(t);
		} catch (UnableToPutTileInCase e) {
			if (flag == true) {
				this.isFirst = false;
			}
			throw e;
		} catch (WrongSelectionException e) {
			if (flag == true) {
				this.isFirst = false;
			}
			throw e;
		}
		return ret;
	}


	public LinkedList<Tile> pick() throws UnableToPutTileInCase, EmptyLocationException {
		int range = ((Player.id * 2) + 1);
		int source = -1;
		int color = -1;
		while (color < 0 || color > 4 || source < 0 || source > range) {
			System.out.println("Select which fabric you want to pick from 1 to " + range + ", or 0 for the center.");
			source = Game.sc.nextInt();
			System.out.println(
					"Select which color you want to pick : 0 = Red, 1 = Blue, 2 = Yellow, 3 = Black, 4 = White");
			color = Game.sc.nextInt();
			if (color < 0 || color > 4 || source < 0 || source > range) {
				System.out.println("Your can't chose this.");
			}
		}
		TType type = typeFromInt(color);
		System.out.println("You chose from :");
		if (source == 0) {
			Center loc = Game.getShared().getCenter();
			loc.display();
			return loc.take(type);
		} else {
			Fabric loc = Game.getShared().getFabrics()[source - 1];
			loc.display();
			if (loc.isEmpty()) {
				System.out.println("You chose from an empty location");
				throw new EmptyLocationException();
			}
			return loc.take(type);
		}
	}

	public static class EmptyLocationException extends Exception {
		private static final long serialVersionUID = 7081801135344952962L;
	}

	private boolean put(LinkedList<Tile> t) throws UnableToPutTileInCase, WrongSelectionException {
		int line = -1;
		while (line < 1 || line > 6) {
			System.out.println("Select in which line you want to play, from 1 to 5. Choose 6 for the floor.");
			line = Game.sc.nextInt();
			if (line < 1 || line > 6) {
				System.out.println("Your can't chose this.");
			}
		}
		return lines.put(t, line - 1);
	}

	public TType typeFromInt(int i) {
		switch (i) {
		case 0:
			return TType.Red;
		case 1:
			return TType.Blue;
		case 2:
			return TType.Yellow;
		case 3:
			return TType.Black;
		case 4:
			return TType.White;
		}
		return null;
	}

	public void buildWall() throws UnableToPutTileInCase {
		score += lines.buildWall();
		floor.resetFloor();
		handStock = null;
	}
	public void endOfTurn() {
		score += floor.endOfTurn();
	}

	public boolean playerEnded() {
		return wall.oneLineFull();
	}
}
