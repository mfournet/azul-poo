package datamodel.src;

public enum TType {
	Red(Color.RED_BACKGROUND_BRIGHT, "Red", new TRest().modul("Red")),
	Blue(Color.BLUE_BACKGROUND_BRIGHT, "Blue", new TRest().modul("Blue")),
	Yellow(Color.YELLOW_BACKGROUND_BRIGHT, "Yellow", new TRest().modul("Yellow")),
	White(Color.WHITE_BACKGROUND_BRIGHT, "White", new TRest().modul("White")),
	Black(Color.BLACK_BACKGROUND_BRIGHT, "Black", new TRest().modul("Black")),
	FirstPlayer(Color.GREEN_BACKGROUND_BRIGHT, "FirstPlayer", new TRest().modul("All")),
	Joker(Color.MAGENTA_BACKGROUND_BRIGHT, "Joker", new TRest().modul("Colors"));

	final private Color color;
	final private String colorS;
	final private TRest rest;

	TType(Color color, String s, TRest r) {
		this.color = color;
		this.colorS = s;
		rest = r;
	}

	public Color getColor() {
		return color;
	}

	public char getFSTile() {
		return this.colorS.charAt(0);
	}
	
	public String getColorS() {
		return this.colorS;
	}
	
	public TRest getRest() {
		return rest;
	}

	public static TType backGroundToTType(java.awt.Color color) {

		if(color == java.awt.Color.RED) return TType.Red;
		if(color == java.awt.Color.BLUE) return TType.Blue;
		if(color == java.awt.Color.YELLOW) return TType.Yellow;
		if(color == java.awt.Color.WHITE) return TType.White;
		if(color == java.awt.Color.BLACK) return TType.Black;
		if(color == java.awt.Color.GREEN) return TType.FirstPlayer;
		if(color == java.awt.Color.MAGENTA) return TType.Joker;

		else return null;
	}
}
