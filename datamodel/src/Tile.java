package datamodel.src;

public class Tile {
	private final TType type; // 0 is FirstPlayer Tile, 1 to 5 are colours and 6 is joker

	public Tile(TType t) {
		type = t;
	}

	public TType getType() {
		return type;
	}

	public void display() {
		System.out.print(type.getColor());
		System.out.print(Color.BLACK_BOLD);
		System.out.print(" "+type.getFSTile()+" ");
		System.out.print(Color.RESET);
		System.out.print("	");
	}
}
