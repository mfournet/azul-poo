JFLAGS = -d "class/"
JC = javac
FOLD = class
.SUFFIXES: .java .class
.java.class:
		$(JC) $(JFLAGS) $*.java

CLASSES = \
		datamodel/src/Azul.java \
		datamodel/src/Game.java \
		datamodel/src/Bag.java \
		datamodel/src/Case.java \
		datamodel/src/Center.java \
		datamodel/src/Color.java \
		datamodel/src/Fabric.java \
		datamodel/src/Lines.java \
		datamodel/src/Player.java \
		datamodel/src/Shared.java \
		datamodel/src/TRest.java \
		datamodel/src/TType.java \
		datamodel/src/Tile.java \
		datamodel/src/Wall.java \
		view/src/GameView.java \
		view/src/MainView.java \
		view/src/MouseEvents.java

build: pre-build classes

pre-build:
		mkdir $(FOLD)

classes: $(CLASSES:.java=.class)

run:
		java -classpath $(FOLD)/ datamodel.src.Azul

b-run: build run

clean:
		rm -fr $(FOLD)
